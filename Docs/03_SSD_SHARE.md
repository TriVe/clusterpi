# Mount disks and share disks from master to slaves with NFS

## How to use

In the Ansible folder, launch the playbook using the following command:

`ansible-playbook playbooks/03_SSD_SHARE.yml -e "@variables.yml" --ask-become-pass`


## Purpose of the playbook

- Mount disks (HDD/SSD)
- Share disks with NFS export on the `master` host
- Mount shared disks on each `worker` host

## What the playbook needs

The playbook use the variable `cluster_disks` from the variables.yml file.

```yaml
cluster_disks:
  - disk_name: EXT
    mount_path: /mnt/ssd1
  - disk_name: Portable SSD T5
    mount_path: /mnt/ssd2
```

## What the playbook does

These steps are done on each raspberry pis consecutively

## Configure the SSD disk share

### Find the disk name (drive)

```bash
$ sudo fdisk -l

Disk /dev/ram0: 4 MiB, 4194304 bytes, 8192 sectors
Units: sectors of 1 * 512 = 512 bytes
(...)
Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Disk model: Portable SSD T5
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 33553920 bytes
Disklabel type: dos
Disk identifier: 0x41d0909f
```

### Create a partition

```bash
$ sudo mkfs.ext4 /dev/sda

mke2fs 1.44.5 (15-Dec-2018)
/dev/sda contains a ext4 file system
    last mounted on /mnt/ssd on Mon Sep  9 21:06:47 2019
Proceed anyway? (y,N) y
Creating filesystem with 58609664 4k blocks and 14655488 inodes
Filesystem UUID: 5c3a8481-682c-4834-9814-17dba166f591
Superblock backups stored on blocks:
    32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
    4096000, 7962624, 11239424, 20480000, 23887872

Allocating group tables: done
Writing inode tables: done
Creating journal (262144 blocks):
done
Writing superblocks and filesystem accounting information: done
```

### Manually mount the disk

```bash
$ sudo mkdir /mnt/ssd
$ sudo chown -R pi:pi /mnt/ssd/
$ sudo mount /dev/sda /mnt/ssd
```

### Automatically mount the disk on startup

Next step consists to configure fstab to automatically mount the disk when the system starts.

You first need to find the Unique ID of the disk using the command blkid.

```bash
$ sudo blkid

/dev/mmcblk0p1: LABEL_FATBOOT="boot" LABEL="boot" UUID="F661-303B" TYPE="vfat" PARTUUID="a91dd8a2-01"
/dev/mmcblk0p2: LABEL="rootfs" UUID="8d008fde-f12a-47f7-8519-197ea707d3d4" TYPE="ext4" PARTUUID="a91dd8a2-02"
/dev/mmcblk0: PTUUID="a91dd8a2" PTTYPE="dos"
/dev/sda: UUID="0ac98c2c-8c32-476b-9009-ffca123a2654" TYPE="ext4"
```

Our SSD located in /dev/sda has a unique ID 0ac98c2c-8c32-476b-9009-ffca123a2654.

Edit the file /etc/fstab and add the following line to configure auto-mount of the disk on startup.

```bash
$ sudo vi /etc/fstab
```

Add this line at the end:

```bash
UUID=0ac98c2c-8c32-476b-9009-ffca123a2654 /mnt/ssd ext4 defaults 0 0
```

Reboot the system

```bash
$ sudo reboot
```

You can verify the disk is correctly mounted on startup with the following command:

```bash
$ df -ha /dev/sda

Filesystem      Size  Used Avail Use% Mounted on
/dev/sda        458G   73M  435G   1% /mnt/ssd
```

## Share via NFS Server (master only)

### Install the required dependencies

```bash
$ sudo apt-get install nfs-kernel-server -y
```

### Configure the NFS server

Edit the file /etc/exports and add the following line

```bash
$ sudo vi /etc/exports

/mnt/ssd *(rw,no_root_squash,insecure,async,no_subtree_check,anonuid=1000,anongid=1000)
```

### Start the NFS Server

```bash
$ sudo exportfs -ra
```

## Mount the NFS share (workers only)

### Install the necessary dependencies

```bash
$ sudo apt-get install nfs-common -y
```

### Create the directory to mounty the NFS Share

Create the directory /mnt/ssd and set the ownership to pi

```bash
$ sudo mkdir /mnt/ssd
$ sudo chown -R pi:pi /mnt/ssd/
```

### Configure auto-mount of the NFS Share

In this step, we will edit /etc/fstab to tell the OS to automatically mount the NFS share into the directory /mnt/ssd when the machine starts.

```bash
$ sudo vi /etc/fstab
```

Add the following line where 192.168.0.22:/mnt/ssd is the IP of kube-master and the NFS share path.

```bash
192.168.0.22:/mnt/ssd   /mnt/ssd   nfs    rw  0  0
```

### Reboot the system

```bash
$ sudo reboot
```
