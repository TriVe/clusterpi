# Prepare OS, install and configure Ansible

## Configure Ubuntu and install Ansible

As a Windows user I use Ubuntu with WSL but any linux distribution should work

Install python, create virtualenv and install Ansible (passlib is used to create hash from password)

```bash
sudo apt-get install -y gcc git curl libssl-dev libffi-dev python3 python3-dev python3-pip
sudo pip3 install virtualenv

virtualenv .ansible299 # create the virtualenv
source ~/.ansible299/bin/activate # Activate the new virtualenv
which python # Check that the current python interpreter is the virtualenv one
pip install ansible==2.9.9 ansible-lint==4.0.1 jmespath # Install ansible in the virtualenv
pip install passlib # Install passlib in the virtualenv
ansible --version # Check the ansible version
```

To connect on the raspberry pi fresh install, you must install sshpass:
`sudo apt-get install  -y`

If you go onto the Ansible directory of this project and use the command `ansible --version`, you should have:

```bash
(.ansible299) trive@MY-DESKTOP:/somewhere/ClusterPi/Ansible$ ansible --version
ansible 2.9.9
  config file = /somewhere/ClusterPi/Ansible/ansible.cfg
  configured module search path = ['/home/trive/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/trive/.ansible299/lib/python3.8/site-packages/ansible
  executable location = /home/trive/.ansible299/bin/ansible
  python version = 3.8.2 (default, Apr 27 2020, 15:53:34) [GCC 9.3.0]
```

⚠ It's important to note that the config file of the project is used and that the python interpreter used is the virtualenv one.

## Create password for users (pi and custom users)

It's time to create the new password for pi and other users

Use the command:

```python
python3 -c 'import crypt; print(crypt.crypt("ThePassword", crypt.mksalt(crypt.METHOD_SHA512)))'
$6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
```

Each generated password must be added to the Ansible\variables.yml file (see examples in the file)

```yaml
cluster_users:
  - name: pi
    password: $6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
  - name: mysuperuser
    password: $6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
```

## Handle the SSH connection with the raspberry-pi

You must have generated a SSH key with `ssh_keygen` with the default file name.

Copy the public key (id_rsa.pub) into the `Ansible\files\` directory (this key is currently in the `~/.ssh` directory)

To test the connection with a single fresh installed pi, switch on only one raspberry-pi and use the following playbook:

```bash
ansible-playbook connection_test_single.yml

...
TASK [Raspberrypi informations] *****************************************************************************************************************************ok: [raspberrypi] => {
    "msg": "System raspberrypi is a Raspberry Pi 4 Model B Rev 1.2\u0000"
}
...
```

## Troubleshouting

### SSH connections issues

If you get the following message:

```bash
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the RSA key sent by the remote host is
bx:fa:bx:b4:51:fe:xe:c7:1f:xx:ff:bf:4a:47:68:4a.
Please contact your system administrator.
Add correct host key in /Users/myname/.ssh/known_hosts to get rid of this message.
Offending RSA key in /Users/myname/.ssh/known_hosts:1
RSA host key for 192.168.xx.xx has changed and you have requested strict checking.
Host key verification failed.
```

It probably means that just reinstalled your system on the pi (flash fresh OS on sd card) and that a ssh key for the host is allready present in the `known_hosts` local file, you must clean this file (remove the host entry in the file)

In theory, this should not happen as the network playbook take care to remove host entry in the `known_hosts` local file.
