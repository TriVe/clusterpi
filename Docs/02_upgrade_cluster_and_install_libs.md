# OS update and tools installation

## How to use

In the Ansible folder, launch the playbook using the following command:

`ansible-playbook playbooks/02_upgrade_cluster_and_install_libs.yml -e "@variables.yml" --ask-become-pass`

## Purpose of the playbook

- Update each host
- Install needed tools (git, python3, pip...)

## What the playbook needs

This playbook does not need extra variable.

## What the playbook does

These steps are done on each raspberry pis consecutively

### Add pi hostname  to local SSH known hosts

### Upgrade the system

```bash
$ sudo apt-get update && sudo apt-get upgrade -y

Get:1 http://raspbian.raspberrypi.org/raspbian buster InRelease [15.0 kB]
Get:2 http://archive.raspberrypi.org/debian buster InRelease [25.1 kB]
Get:3 http://raspbian.raspberrypi.org/raspbian buster/main armhf Packages [13.0 MB]
Get:4 http://archive.raspberrypi.org/debian buster/main armhf Packages [261 kB]
99% [3 Packages store 0 B]
(...)  
```

### Enable container features

```bash
$ sudo vi /boot/cmdline.txt
```

and add the following properties at the end **of the line**:

```bash
cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
```

### Firewall

Switch Debian firewall to legacy config:

```bash
$ update-alternatives --set iptables /usr/sbin/iptables-legacy
$ update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
```

### install git and python3

### create python and pip symbolic link
