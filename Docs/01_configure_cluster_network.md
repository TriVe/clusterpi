# Cluster network configuration

## How to use

In the Ansible folder, launch the playbook using the following command:

`ansible-playbook playbooks/01_configure_cluster_network.yml -e "@variables.yml" --ask-become-pass`

## Purpose of the playbook

For each host:

- Change the locals (with timezone)
- Configure network (ip/hostmane)
- Create users with sudo rights
- Copy ssh-keys for futur connections

## What the playbook needs

The playbook use the variables `cluster_users`, `config_system_locale` and `config_system_language` from the variables.yml file.

```yaml
config_system_locale: 'fr_FR.UTF-8'
config_system_language: 'en_US.UTF-8'

cluster_users:
  - name: pi
    password: $6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
  - name: mysuperuser
    password: $6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
```

## What the playbook does

These steps are done on each raspberry pis consecutively

### Set pi locales

### Set pi timezone

### Change hostname

```bash
$ sudo vi /etc/hostname

kube-master
```

```bash
$ sudo vi /etc/hosts

127.0.0.1       localhost
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters

127.0.1.1       kube-master
```

### Configure a static IP

```bash
$ sudo vi /etc/dhcpcd.conf

interface eth0
static ip_address=192.168.0.<X>/24
static routers=192.168.0.1
static domain_name_servers=1.1.1.1
```

### Change password

```bash
$ passwd

Changing password for pi.
Current password: raspberry
New password: <new_password>
Retype new password: <new_password>
passwd: password updated successfully
```

### Copy public key on pi

Copy each user key on pi for future connections

### Reboot pi

Pi will reboot with new hostname and ip
