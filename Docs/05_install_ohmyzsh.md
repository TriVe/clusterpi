# Install ZSH shell

## How to use

In the Ansible folder, launch the playbook using the following command:

`ansible-playbook playbooks/05_install_ohmyzsh.yml -e "@variables.yml" --ask-become-pass`

## Purpose of the playbook

- Install the ZSH shell on each host and for each users
- Configure the shell and customize it

This playbook use a third-party role [viasite-ansible.zsh](https://galaxy.ansible.com/viasite-ansible/zsh) to install the shell and its plugins.

I choose to use the `powerlevel10k` theme because IMHO, it rocks 🤟

## What the playbook needs

The playbook use the variable `cluster_users` from the variables.yml file.

```yaml
cluster_users:
  - name: pi
    password: $6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
  - name: mysuperuser
    password: $6$0aGW2p70KdYVy5j0$5CfLS/anajdo75TIMgVr3JQa3GdK3cAQjXB52TRcsE3RM.rhN3JW9QIwMd9A4Q0bn27a/yTJnphCHcGAbnGMO.
```

 The file (files/.p10k.zsh) is used to customize the theme and can be adapted to your needs.

## What the playbook does

TODO: add details on playbook tasks
