# Install and configure media namespace

## How to use

In the Ansible folder, launch the playbook using the following command:

`ansible-playbook playbooks/06_install_media_namespace.yml -e "@variables.yml"`

## Purpose of the playbook

The purpose of the playbook is to install a media center and a seedbox as a kubernetes namespace:

- Transmission over vpn to download .torrent
- Jacket over vpn for torrents indexing
- Radarr to find films, send them to transmission and store them
- Sonarr to find TV shows, send them to transmission and store them
- Plex server for media broadcasting

[![Foo](./assets/media_namespace.webp)](https://kauri.io/self-host-your-media-center-on-kubernetes-with-plex-sonarr-radarr-transmission-and-jackett/8ec7c8c6bf4e4cc2a2ed563243998537/a)
[source](https://kauri.io/self-host-your-media-center-on-kubernetes-with-plex-sonarr-radarr-transmission-and-jackett/8ec7c8c6bf4e4cc2a2ed563243998537/a)

## What the playbook needs

The playbook use the variable `cluster_user_email` from the variables.yml file.

```yaml
cluster_user_email: 'foo.bar@provider.com'
```

This role also use it's own variables that should/must be adapted to your needs:

```yaml
# defaults values for k3s-MediaCenter
media_center_apps_data_storage_size: "300Gi"
media_center_apps_data_storage_path: "/mnt/ssd1/media/data"

media_center_config_storage_size: "2Gi"
media_center_config_storage_path: "/mnt/ssd1/media/config"

media_center_media_storage_size: "800Gi"
media_center_media_storage_path: "/mnt/ssd2/media"

VPN_USERNAME: "xxxxxxxxxx"
VPN_PASSWORD: "yyyyyyyyyyyy"
VPN_PROVIDER: "TIGER"
VPN_CONFIG: "CA Toronto"

PLEX_CLAIM_TOKEN: "abcdefghijklmnopqrstuvwxyz"
JACKETT_APIKEY: "" # empty for random

TRANSMISSION_VERSION: "2.12-armhf" # https://hub.docker.com/r/haugene/transmission-openvpn/tags
JACKETT_VERSION: "v0.16.916" # https://hub.docker.com/r/gjeanmart/jackettvpn/tags
RADARR_VERSION: "arm32v7-3.0.0.3374-ls18" # https://hub.docker.com/r/linuxserver/radarr/tags
SONARR_VERSION: "arm32v7-3.0.3.906-ls36" # https://hub.docker.com/r/linuxserver/sonarr/tags
PLEX_VERSION: "arm32v7-1.19.5.3112-b23ab3896-ls112" # https://hub.docker.com/r/linuxserver/plex/tags
```

For the VPN, you must copy the .ovpn file and the vpn certificate ca.crt in the directory `roles\k3s-MediaCenter\files\openvpn`

It's also possible to provide your own Jackett indexers by copying indexers json files in the directory `roles\k3s-MediaCenter\files\jackett\indexers`

## What the playbook does

TODO: add details on playbook tasks
