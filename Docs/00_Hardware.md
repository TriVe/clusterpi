# Cluster assembly (Hardware  and raspios-buster-lite os image)

My cluster is composed of 4 raspberry-pi 4:

| Hostname     |      IP       |
| ------------ | :-----------: |
| kube-master  | 192.168.1.125 |
| kube-worker1 | 192.168.1.126 |
| kube-worker2 | 192.168.1.127 |
| kube-worker3 | 192.168.1.128 |

## Hardware  assembly

TODO: Add details on the cluster hardware

## Isnatll OS

I use 128GB SD cards (that in my opinion are too large, 32GO should be enough)

### Install the raspios-buster-lite image on the SD card

On each of the SD card:

- I used `Etcher` to flash the OS image `2020-05-27-raspios-buster-lite-armhf`
- I created a `ssh` empty file on the root of the SD card (on the boot partition) to activate the ssh by default.

❗️ I recently decided to include the ssh file into my raspios image to gain time.

### Install Ubuntu server 64bits image on the SD card

As I use Rpi4, I decided do use a 64 bits OS so I choose to use ubuntu server.

- I used `Etcher` to flash the OS image `ubuntu-20.04.2-preinstalled-server-arm64+raspi.img.xz`
- As the default user and hostname are not the same as for raspbian, I had to modify the user-data file (on the boot partition)

```yaml
# On first boot, set the (default) ubuntu user's password to "ubuntu" and
# expire user passwords
chpasswd:
  expire: false
  list:
    - ubuntu:ubuntu
    - pi:raspberry

# Enable password authentication with the SSH daemon
ssh_pwauth: true

## Add users and groups to the system, and import keys with the ssh-import-id
## utility
groups:
- pi

users:
- name: pi
  primary_group: pi
  shell: /bin/bash
  sudo: ALL=(ALL) NOPASSWD:ALL
  groups: users,pi
  lock_passwd: false
  passwd: raspberry

```

So we created a pi user with root privileges and the pi group
