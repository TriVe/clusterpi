# What's next

## Features

- [X] Install Oh-My-Zsh on each pi
- [ ] Use SickChill to replace sonarr/radarr
- [ ] Use Joal to fake seeding torrent
- [X] Deploy NextCloud on Kuberbetes: The self-hosted Dropbox
- [X] Self-host Pi-Hole on Kubernetes and block ads and trackers at the network level
- [ ] Deploy Prometheus and Grafana to monitor a Kubernetes cluster
- [ ] Create documentation for pihole and nextcloud

## Fix

- [X] Use variables for transmission openvpn config
- [X] Set timezone using  `sudo timedatectl set-timezone Europe/Paris` (use variable for timezone)
- [ ] Fix cert-manager chart that does not work

## Refactoring

- [x] Split `configure_cluster` playbook in two and move it to the playbook directory
- [x] Move `01_prepare` and `02_prepare` files into the `tasks` directory and rename it
- [x] Set specific versions for metallb and cert-manager helm charts

