---
# tasks file for k3s-MediaCenter
- name: Install media namespace
  block:
    # Copy templates to host
    - name: Create temp directory
      file:
        path: /tmp/kube
        state: directory
    - name: copy template files onto master
      template:
        src: '{{ item.src }}'
        dest: "/tmp/kube/{{ item.path | regex_replace('.j2','') }}"
      with_filetree: '../templates'
      when: item.state == 'file'

    - name: Creates shared folder
      become: yes
      file:
        path: "{{ media_center_media_storage_path }}"
        state: directory
        owner: pi
        group: pi
        mode: 0775
    - name: Mount nfs
      become: yes
      command: "mount -t nfs {{ nfs_distant_ip }}:{{ media_center_media_storage_path }} {{ media_center_media_storage_path }}"

    # Create media Namespace
    - name: Delete namespace media
      command: "kubectl delete namespace media"
      ignore_errors: yes
    - name: Delete media-data PV
      command: "kubectl delete pv media-data --grace-period=0 --force"
      ignore_errors: yes
    - name: Delete apps-config PV
      command: "kubectl delete pv apps-config --grace-period=0 --force"
      ignore_errors: yes
    - name: Delete apps-data PV
      command: "kubectl delete pv apps-data --grace-period=0 --force"
      ignore_errors: yes
    - name: Create namespace media
      command: "kubectl create namespace media"

    # Create PV & PVC media-ssd
    - name: Create PV folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}'
        - '{{ media_center_media_storage_path }}'
        - '{{ media_center_apps_data_storage_path }}'
    - name: Create PV
      shell: "kubectl apply -f /tmp/kube/media.persistentvolume.yml"
    - name: Create PVC
      shell: "kubectl apply -f /tmp/kube/media.persistentvolumeclaim.yml"

    # Deploy ingress
    - name: Deploy the ingress
      shell: "kubectl apply -f /tmp/kube/media.ingress.yml"

    # Install transmission
    - name: Create folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_apps_data_storage_path }}/downloads'
        - '{{ media_center_apps_data_storage_path }}/transmission/watch'
        - '{{ media_center_apps_data_storage_path }}/transmission/incomplete'
    - name: Add Bananaspliff helm repo
      command: helm repo add bananaspliff https://bananaspliff.github.io/geek-charts
    - name: Update helm repos
      command: helm repo update
    - name: Create a Kubernetes secret to store VPN password
      shell: "kubectl create secret generic openvpn --from-literal username={{ VPN_USERNAME }} --from-literal password={{ VPN_PASSWORD }} --namespace media"
    - name: Install the chart bananaspliff/transmission-openvpn
      command: helm install transmission bananaspliff/transmission-openvpn --values /tmp/kube/media.transmission-openvpn.values.yml --namespace media

    # Install FlareSolverr
    - name: Create flaresolverr service and deployment
      shell: "kubectl apply -f /tmp/kube/media.flaresolverr.allinone.yaml -n media"

    # Install Jackett
    - name: Create jackett folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}/jackett/openvpn'
        - '{{ media_center_config_storage_path }}/jackett/Jackett'
        - '{{ media_center_apps_data_storage_path }}/jackett'
    - name: Copy the Jackett conf files
      copy:
        src: "{{ item.src }}"
        dest: "{{ item.dest }}"
        remote_src: yes
      with_items:
        - { src: '/tmp/kube/media.jackett.credentials.conf', dest: '{{ media_center_config_storage_path }}/jackett/openvpn/credentials.conf' }
        - { src: '/tmp/kube/media.jackett.ServerConfig.json', dest: '{{ media_center_config_storage_path }}/jackett/Jackett/ServerConfig.json' }
    - name: Copy the openvpn conf files
      copy:
        src: ../files/openvpn/
        dest: '{{ media_center_config_storage_path }}//jackett/openvpn'
    - name: Copy indexers
      copy:
        src: ../files/jackett/indexers/
        dest: '{{ media_center_config_storage_path }}/jackett/Jackett/Indexers'
    - name: Install the chart bananaspliff/jackett
      command: helm install jackett bananaspliff/jackett --values /tmp/kube/media.jackett.values.yml --namespace media


    # Install Sonarr
    - name: Create sonarr folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}/sonarr'
        - '{{ media_center_apps_data_storage_path }}/downloads'
        - '{{ media_center_media_storage_path }}/library/tv'
    - name: Copy the config file
      copy:
        src: /tmp/kube/media.sonarr.config.xml
        dest: '{{ media_center_config_storage_path }}/sonarr/config.xml'
        remote_src: yes
        force: no
    - name: Install the chart bananaspliff/sonarr
      command: helm install sonarr bananaspliff/sonarr --values /tmp/kube/media.sonarr.values.yml --namespace media


    # Install Radarr
    - name: Create radarr folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}/radarr'
        - '{{ media_center_apps_data_storage_path }}/downloads'
        - '{{ media_center_media_storage_path }}/library/movies'
    - name: Copy the config file
      copy:
        src: /tmp/kube/media.radarr.config.xml
        dest: '{{ media_center_config_storage_path }}/radarr/config.xml'
        remote_src: yes
    - name: Install the chart bananaspliff/radarr
      command: helm install radarr bananaspliff/radarr --values /tmp/kube/media.radarr.values.yml --namespace media

    # Install Lidarr
    - name: Create lidarr folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}/lidarr'
        - '{{ media_center_apps_data_storage_path }}/downloads'
        - '{{ media_center_media_storage_path }}/library/music'
    - name: Copy the config file
      copy:
        src: /tmp/kube/media.lidarr.config.xml
        dest: '{{ media_center_config_storage_path }}/lidarr/config.xml'
        remote_src: yes
    - name: Create lidarr service and deployment
      shell: "kubectl apply -f /tmp/kube/media.lidarr.allinone.yaml -n media"

    # Install Readarr
    - name: Create readarr folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}/readarr/app'
        - '{{ media_center_apps_data_storage_path }}/downloads'
        - '{{ media_center_media_storage_path }}/library/books'
        - '{{ media_center_media_storage_path }}/library/audiobooks'
    - name: Copy the config file
      copy:
        src: /tmp/kube/media.readarr.config.xml
        dest: '{{ media_center_config_storage_path }}/readarr/app/config.xml'
        remote_src: yes
    - name: Create readarr service and deployment
      shell: "kubectl apply -f /tmp/kube/media.readarr.allinone.yaml -n media"

    # Install Joal
    - name: Create joal folders
      file:
        path: '{{ item }}'
        state: directory
        recurse: yes
      with_items:
        - '{{ media_center_config_storage_path }}/joal'
    - name: Copy joal files
      copy:
        src: ../files/joal
        dest: '{{ media_center_config_storage_path }}'
    - name: Create joal service and deployment
      shell: "kubectl apply -f /tmp/kube/media.Joal.allinone.yaml -n media"


    # Install Plex
#    - name: Clone kube-plex repository
#      git:
#        repo: https://github.com/munnerz/kube-plex.git
#        dest: /tmp/kube/kube-plex
#    - name: Install the chart kube-plex
#      command: helm install plex /tmp/kube/kube-plex/charts/kube-plex/ --values /tmp/kube/media.plex.values.yml --namespace media


    # Create ServiceAccount and CronJob to restart transmission weekly
    - name: Create ServiceAccount, Role and RoleBinding
      shell: "kubectl apply -f /tmp/kube/media.serviceAccount.yml -n media"
    - name: Create CronJob to restart transmission weekly
      shell: "kubectl apply -f /tmp/kube/media.cronjob.yml -n media"

  always:
    - name: Remove kube temp directory
      file:
        path: /tmp/kube
        state: absent
    - name: Unmount nfs shared folder
      become: yes
      command: "umount {{ media_center_media_storage_path }}"
