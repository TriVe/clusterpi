# Cluster pi

The purpose of this repository is to ba able to configure my raspberry-pi cluster idempotently.

I choose Ansible and created many playbooks for each step of the configuration and softwares installations.

I also choose to use light kubernetes version (k3s) to handle all apps that will be deployed on the cluster (media-center, cloud...)

Almost every playbooks are translated to `ansible` from the articles collection `Build your very own self-hosting platform with Raspberry Pi and Kubernetes` (cf. [1])

## Prerequisite

You should read the two following documents to configure base cluster and environment:

01 - [Cluster assembly (Hw and raspios-buster-lite os image)](Docs/00_Hardware.md)

02 - [Installation ansible et prerequis à l'utilisation des playbooks)](Docs/00_bis_ansible_and_prerequisite.md)

Once the cluster and environment are up and running and before using playbooks, you **must**:

- Install the third-party roles using the requirements file: `ansible-galaxy role install -r requirements.yml` (the `asnsible.cfg` file makes the role to install locally).
- Adapt the `variables.yml` vars file to your needs (it will be use as extra vars file).
- Adapt the `hosts-cluster` inventory file to your cluster (add/remove workers, change ips...).

## All-In-One install

To install the whole cluster in one shot, you just need to use the master playbook `Install_Cluster.yml`:
`ansible-playbook Install_Cluster.yml -e "@variables.yml" --ask-become-pass`

⚠ The first 3 steps need user interaction like switch on/off the pis, connect SSD/HDD on master.

## Step by step install

Here is the playbokk schedulling.
`As an example, you could not install nextcloud before installing k3s (and that make sense 😄) but you can install ohmyzsh at any time after upgrading pis and installing libs.`

<p align="center">
    <img src="Docs/assets/Install_scheduling.png" alt="Install_scheduling" width="500"/>
</p>

- [Cluster network configuration](Docs/01_configure_cluster_network.md)

- [OS update and tools installation (python...)](Docs/02_upgrade_cluster_and_install_libs.md)

- [Mount disks and share disks from master to slaves with NFS](Docs/03_SSD_SHARE.md)

- [Install ZSH shell](Docs/05_install_ohmyzsh.md)

- [Install and configure kubernetes (k3s)](Docs/04_INSTALL_K3S.md)

- [Install and configure media namespace](Docs/06_install_media_namespace.md)

⚠ The playbooks using `--ask-become-pass` needs the pi user password to be provided.

## Sources

- [Maxence's technical corner](http://m-button.blogspot.com/2018/02/building-small-raspberry-pi-cluster-and.html)
- [Build your very own self-hosting platform with Raspberry Pi and Kubernetes](https://kauri.io/build-your-very-own-self-hosting-platform-with-raspberry-pi-and-kubernetes-introduction/1229f21044ef4bff8df35875d6803776/a)
- [Build a Kubernetes cluster using k3s via Ansible](https://github.com/itwars/k3s-ansible)

[1]: https://kauri.io/build-your-very-own-self-hosting-platform-with-raspberry-pi-and-kubernetes-introduction/1229f21044ef4bff8df35875d6803776/a
